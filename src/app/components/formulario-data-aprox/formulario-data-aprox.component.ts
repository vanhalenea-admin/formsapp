import { browser } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-formulario-data-aprox',
  templateUrl: './formulario-data-aprox.component.html',
  styleUrls: ['./formulario-data-aprox.component.css']
})
export class FormularioDataAproxComponent implements OnInit {

  forma: FormGroup;
  usuario: any = {
    nombrecompleto: {
      nombre: 'fernando',
      apellido: 'herrera'
    },
    correo: 'asd@asd.cl',
    pasatiempos: []
  }

  constructor() { 
    this.forma = new FormGroup({
      'nombrecompleto': new FormGroup({
             'nombre' : new FormControl('',[Validators.required,Validators.minLength(3)]),
             'apellido' : new FormControl('',[Validators.required,this.noRojas])
      })
        ,
        'correo': new FormControl('',[Validators.required, Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")]),  
        'pasatiempos': new FormArray([

        ]),
        'username' : new FormControl('',[Validators.required],this.userNameAsyncValidate),
        'password1' : new FormControl('',[Validators.required]),
        'password2' : new FormControl('',[Validators.required])
      });

      //this.forma.setValue(this.usuario);

      this.forma.valueChanges.subscribe (data =>{
        console.log(data)
      })
      /*
        this.forma.controls['username'].valueChanges.subscribe (data =>{
        console.log(data)
      })
      */
  }

  ngOnInit() {
  }

  onSubmit() {

    this.forma.reset(
      {
        nombrecompleto: {
          nombre: "",
          apellido: ""
        },
        correo: ""
      }
    )
  }

  agregarPasatiempos() {
    
    (<FormArray>this.forma.controls['pasatiempos'].push(
      new FormControl('Dormir',Validators.required)
    )

    )
  }
/**
 * Custom validator
 */


  noRojas (control: FormControl ) : { [s:string]: boolean}  {

    if(control.value === "rojas") {
      return {
        norojas: true
      }
    }

    return null;

  }


  userNameAsyncValidate(control: FormControl): Promise<any> | Observable<any>{

    let promesa = new Promise(

    (resolve, reject) => {

        setTimeout ( () => {
          if( control.value === "strider"){
            resolve( { existe: true})
          } else {
            resolve (null)
          }

        }, 3000 ) 

      }
    )
      return promesa;
  }

}
